package org.opencds.opencds_bounce;

import org.hl7.fhir.instance.model.Bundle;
import org.hl7.fhir.instance.model.Parameters;
import org.hl7.fhir.instance.model.Resource;
import org.opencds.hooks.model.request.CdsRequest;
import org.opencds.hooks.model.response.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.UUID;

public class BounceUtil {
    public static CdsResponse getResponse(Parameters params) {
        List<Suggestion> suggestions = new ArrayList<Suggestion>();
        Suggestion suggestion = new Suggestion();
        suggestion.setLabel("bounce");
        suggestion.setUuid(UUID.randomUUID().toString());

        List<Action> actions = new ArrayList<Action>();
        Action action = new Action();
        action.setResource(params);
        actions.add(action);

        suggestion.setCreate(actions);
        suggestions.add(suggestion);

        Card card = new Card();
        card.setSummary("Evaluation successful.");
        card.setDetail("See suggestions for bounce content.");
        card.setIndicator(Indicator.INFO);
        card.setSuggestions(suggestions);

        CdsResponse cdsResponse = new CdsResponse();
        cdsResponse.addCard(card);
        return cdsResponse;
    }

    public static Parameters getParams(CdsRequest cdsRequest) {
        Parameters params = new Parameters();
        Map<String, Bundle.BundleEntryComponent> prefetch = cdsRequest.getPrefetch();

        for (Map.Entry<String, Bundle.BundleEntryComponent> entry : prefetch.entrySet()) {
            Bundle.BundleEntryComponent entryComponent = entry.getValue();
            Resource resource = entryComponent.getResource();
            Parameters.ParametersParameterComponent comp = new Parameters.ParametersParameterComponent();
            comp.setResource(resource);
            params.addParameter(comp);
        }
        return params;
    }
}
