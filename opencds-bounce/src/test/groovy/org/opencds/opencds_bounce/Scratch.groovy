package org.opencds.opencds_bounce

import org.hl7.fhir.instance.model.Parameters
import org.opencds.hooks.lib.JsonUtil
import org.opencds.hooks.model.request.CdsRequest
import org.opencds.hooks.model.response.CdsResponse
import spock.lang.Specification

class Scratch extends Specification {

    def "my scratch test"() {

        when:

        JsonUtil jsonUtil = new JsonUtil();

        Parameters params = new Parameters();

        CdsRequest cdsRequest = jsonUtil.fromJson(
                new File('src/test/resources/org/opencds/opencds_bounce/hooks-request-test.json').text,
                CdsRequest.class
        )

        Parameters parameters = BounceUtil.getParams(cdsRequest)
        CdsResponse cdsResponse = BounceUtil.getResponse(parameters)

        def json = jsonUtil.toJson(cdsResponse)

        then:

        println json
    }
}
