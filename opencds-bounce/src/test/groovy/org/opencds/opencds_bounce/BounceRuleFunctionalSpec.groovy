package org.opencds.opencds_bounce

import groovy.json.JsonSlurper
import org.opencds.common.utilities.DateUtility
import org.opencds.drools.util.TrackingAgendaEventListener
import org.opencds.hooks.evaluation.service.ResourceListBuilder
import org.opencds.hooks.lib.JsonUtil
import org.opencds.hooks.model.request.CdsRequest

import org.kie.api.KieServices
import org.kie.api.builder.KieModule
import org.kie.api.builder.KieRepository
import org.kie.api.command.Command;
import org.kie.api.event.rule.AgendaEventListener
import org.kie.api.runtime.ExecutionResults
import org.kie.api.runtime.KieContainer;
import org.kie.api.runtime.StatelessKieSession
import org.kie.internal.command.CommandFactory
import org.skyscreamer.jsonassert.JSONAssert

import spock.lang.Shared
import spock.lang.Specification

import java.nio.file.Paths

public class BounceRuleFunctionalSpec extends Specification {

	private static final String EVAL_TIME = "evalTime"
	private static final String CLIENT_LANG = "clientLanguage"
	private static final String CLIENT_TZ_OFFSET = "clientTimeZoneOffset"
	private static final String FOCAL_PERSON_ID = "focalPersonId"
	private static final String ASSERTIONS = "assertions"
	private static final String NAMED_OBJECTS = "namedObjects"

	@Shared
	KieContainer kieContainer
	@Shared
	JsonSlurper jsonSlurper

	String primaryProcess = "opencds-bounce.PrimaryProcess"
	String clientLanguage
	String clientTimeZoneOffset

	/****************************************************/

	def "001"() {

		given:
		def payload = "hooks-request-test.json"

		when:
		def result = exec(payload)
		println result

		then:
		result
		//JSONAssert.assertEquals(expected("hooks-request-test-response.json"), result, true)
	}

	/****************************************************/

	def setupSpec() {

		jsonSlurper = new JsonSlurper()

		KieServices ks = KieServices.Factory.get()
		kieContainer = null

		InputStream inputStream = Paths.get('target').toFile().listFiles().find {f -> f.name =~ /jar-with-dependencies/}?.newInputStream()
		org.kie.api.io.Resource resource = ks.getResources().newInputStreamResource(inputStream)
		KieRepository kr = ks.getRepository()
		KieModule kieModule = kr.addKieModule(resource)
		kieContainer = ks.newKieContainer(kieModule.getReleaseId())

	}

	def exec (def fileName ) {
		Map<String, Object> namedObjects = [:]
		List<Command> cmds = new ArrayList<Command<?>>()
		Set<String> assertions = []
		String focalPersonId

		def commonFolder = "src/test/resources/org/opencds/opencds_bounce/"
		def json = new File(commonFolder + fileName).text

		def jsonUtil = new JsonUtil()
		def request = jsonUtil.fromJson(json, CdsRequest.class)

		def builder = new ResourceListBuilder()

		Map<Class<?>, List<?>> allFactLists = builder.buildAllFactLists(request,null)

		 /**
		 * Load the FactLists into Commands: Only ones that are not empty...
		 */
		if (allFactLists != null) {
			for (Map.Entry<Class<?>, List<?>> factListEntry : allFactLists.entrySet()) {
				if (factListEntry.getValue().size() > 0) {
					if ( "FocalPersonId".equals(factListEntry.getKey().getSimpleName()) ) {
						cmds.add(CommandFactory.newSetGlobal(FOCAL_PERSON_ID, factListEntry.getValue().get(0).toString()));
					}
					cmds.add(CommandFactory.newInsertElements((List<?>) factListEntry.getValue(), factListEntry
							.getKey().getSimpleName(), true, null));
				}
			}
		}

		cmds.add(CommandFactory.newSetGlobal(EVAL_TIME, DateUtility.getInstance().getDateFromString("2016-01-04 00:00:00", "yyyy-MM-dd HH:mm:ss")));
		cmds.add(CommandFactory.newSetGlobal(CLIENT_LANG, clientLanguage))
		cmds.add(CommandFactory.newSetGlobal(CLIENT_TZ_OFFSET, clientTimeZoneOffset))
		cmds.add(CommandFactory.newSetGlobal(ASSERTIONS, assertions))
		cmds.add(CommandFactory.newSetGlobal(NAMED_OBJECTS, namedObjects))
		cmds.add(CommandFactory.newSetGlobal(FOCAL_PERSON_ID, focalPersonId))
		cmds.add(CommandFactory.newStartProcess(primaryProcess))

		StatelessKieSession statelessKnowledgeSession = kieContainer.newStatelessKieSession()
		AgendaEventListener agendaEventListener = new TrackingAgendaEventListener();

		statelessKnowledgeSession.addEventListener(agendaEventListener)
		ExecutionResults results = statelessKnowledgeSession.execute(CommandFactory.newBatchExecution(cmds))

		def cdsResponse = namedObjects.get('CdsResponse')
		return jsonUtil.toJson(cdsResponse)
	}

	def expected(String fileName) {
		return new File("src/test/resources/org/opencds/opencds_bounce/${fileName}").text
	}
}